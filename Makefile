FILES = psi a0 psi_2
IMG = $(addprefix math/, $(addsuffix .svg, $(FILES))) img/

all: $(IMG)

hydrogen.pdf: hydrogen.svg $(IMG)
	rsvg-convert -f pdf -o $@ $<
hydrogen.png: hydrogen.svg $(IMG)
	rsvg-convert -f png -o $@ $<

img/:	hydrogen.gp
	gnuplot $<

%.svg : %.ps
	pstoedit -f plot-svg -dt -ssp $< $@

%.ps : %.dvi
	dvips -q -f -D 600 -y 5000 -o $@ $<

%.dvi %.aux %.log : %.tex
	latex -output-directory=math $<

#%.svg: %.pdf
#	pdf2svg $< $@

#%.pdf %.aux %.log: %.tex
#	pdflatex $<

.INTERMEDIATE : $(addprefix math/, $(addsuffix .aux, $(FILES)) $(addsuffix .log, $(FILES)))
