# Hydrogen Atom - Wave Functions

![](../-/jobs/artifacts/main/raw/hydrogen.png?job=build)

[download pdf](../-/jobs/artifacts/main/raw/hydrogen.pdf?job=build)

## Software requirements

* GNU Make (or read [Makefile](Makefile) and manually execute the commands)
* [gnuplot](http://www.gnuplot.info/)
* latex
* dvips
* pstoedit
* [inkscape](https://inkscape.org/) or
  [rsvg-convert](https://github.com/GNOME/librsvg/) to convert [hydrogen.svg](hydrogen.svg) into a pdf

Instead of latex, dvips, pstoedit one also could use
* pdflatex
* [pdf2svg](https://github.com/dawbarton/pdf2svg)

However in contrast to the combination latex -> dvips -> pstoedit,
pdflatex -> pdf2svg does not create predictable ids for the svg elements.

## Render png / pdf

To generate latex formulae and png images linked in [hydrogen.svg](hydrogen.svg):

```sh
make
```

To generate a pdf:

```sh
make hydrogen.pdf
```
