set terminal png crop notransparent enhanced size 1024,1024
set view map
set isosamples 1024, 1024
set samples 1024
unset surface
unset key
unset border
set style function pm3d
set xrange [ -40.0 : 40.0 ] noreverse nowriteback
set yrange [ -40.0 : 40.0 ] noreverse nowriteback
set zrange [ 0.0 : 0.001]
set cbrange [ 0.0 : 0.001 ]
set pm3d at b
set palette model RGB
set palette defined ( 0 "black", 1 "green", 2 "yellow", 5 "white" )

rho(x,y) = sqrt(x*x+y*y)
rho_2(x,y) = x*x+y*y

system "mkdir -p img"

unset tics
unset colorbox

set output 'img/1-0-0.png'
splot exp(-0.5*rho(x,y))/pi

set output 'img/2-0-0.png'
splot (2.-rho(x,y))**2*exp(-rho(x,y))/(32*pi)

set output 'img/2-1-0.png'
splot 1./(32*pi)*y*y*exp(-rho(x,y))

set output 'img/2-1-1.png'
splot 1./(8*8*pi)*x*x*exp(-rho(x,y))

set output 'img/3-0-0.png'
splot (27.-18.*rho(x,y)+2.*rho_2(x,y))**2*exp(-2./3.*rho(x,y)) *1./(81*81*3*pi)

set output 'img/3-1-0.png'
splot 2./(81*81*pi)*((6.-rho(x,y))*y)**2*exp(-2./3.*rho(x,y))

set output 'img/3-1-1.png'
splot 1./(81*81*pi)*((6.-rho(x,y))*x)**2*exp(-2./3.*rho(x,y))

set output 'img/3-2-0.png'
splot 1./(81*81*6*pi)*(3.*y*y-rho_2(x,y))**2*exp(-2./3.*rho(x,y))

set output 'img/3-2-1.png'
splot 1./(81*81*pi)*x*x*y*y*exp(-2./3.*rho(x,y))

set output 'img/3-2-2.png'
splot 1./(162*162*pi)*x*x*x*x*exp(-2./3.*rho(x,y))

set output 'img/4-0-0.png'
splot 1./(2359296*pi) * (rho_2(x,y)*rho(x,y)-24.*rho_2(x,y)+144.*rho(x,y)-192.)**2*exp(-rho(x,y)/2)

set output 'img/4-1-0.png'
splot 1./(1310720*pi) * y*y*(rho_2(x,y)-20.*rho(x,y)+80.)**2*exp(-rho(x,y)/2)

set output 'img/4-1-1.png'
splot 1./(2621440*pi) * x*x*(rho_2(x,y)-20.*rho(x,y)+80.)**2*exp(-rho(x,y)/2)

set output 'img/4-2-0.png'
splot 1./(9437184*pi) * (rho(x,y)-12.)**2*(3.*y*y-rho_2(x,y))**2*exp(-rho(x,y)/2)

set output 'img/4-2-1.png'
splot 1./(1572864*pi) * x*x*y*y*(rho(x,y)-12.)**2*exp(-rho(x,y)/2)

set output 'img/4-2-2.png'
splot 1./(6291456*pi) * x*x*x*x*(rho(x,y)-12.)**2*exp(-rho(x,y)/2)

set output 'img/4-3-0.png'
splot 1./(47185920*pi) * y*y*(5.*y*y-3.*rho_2(x,y))**2*exp(-rho(x,y)/2)

set output 'img/4-3-1.png'
splot 1./(62914560*pi) * x*x*(5.*y*y-rho_2(x,y))**2*exp(-rho(x,y)/2)

set output 'img/4-3-2.png'
splot 1./(6291456*pi) * x*x*x*x*y*y*exp(-rho(x,y)/2)

set output 'img/4-3-3.png'
splot 1./(37748736*pi) * x*x*x*x*x*x*exp(-rho(x,y)/2)
